<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link href="cssUser.css" rel="stylesheet">
</head>

<body>
  <?
  require_once 'dataBase.php';
  require_once 'header.php';
  require_once 'nav.php'; ?>

  <? $query = $db->prepare("SELECT * FROM users WHERE id =:id ");
  $query->execute(array(
    ':id' => filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT),
  ));
  $users = $query->fetch();

  $query = $db->prepare("SELECT * FROM todos WHERE userId =:id ");
  $query->execute(array(
    ':id' => filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT),
  ));
  $todos = $query->fetchall();

  $query = $db->prepare("SELECT * FROM albums WHERE userId =:id ");
  $query->execute(array(
    ':id' => filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT),
  ));
  $albums = $query->fetchall();

  ?>

  <h1 id='titre'>Profil de <span style="color:blue" ><?= $users['name'] ?></span> </h1>

  <div class="container">
    <div class="row justify-content-left">
      <div class="col-4">
        <div class="card shadow-lg  bg-white">
          <h5 class="card-header">INFORMATION PERSONELLES</h5>
          <div class="card-body">
            <h5 class="card-title">Nom : <?= $users['name'] ?></h5>
            <p class="card-text">Login : <?= $users['username'] ?></p>
            <p class="card-text">Email : <?= $users['email'] ?></p>
            <p class="card-text">adresse : <?= $users['addressstreet'] ?> <br> <?= $users['addresssuite'] ?> <br> <?= $users['addresscity'] ?></p>
            <p class="card-text">Téléphone : <?= $users['phone'] ?></p>
            <p class="card-text">Entreprise : <?= $users['companyname'] ?> <br> <?= $users['companycatchPhrase'] ?> <br> <?= $users['companybs'] ?></p>
          </div>
          </div>
        </div>
        <div class="card shadow-lg  bg-white col-4">
          <h5 class="card-header">todoliste</h5>
          <div class="card-body ">
            <? foreach ($todos as $todo) {
              $toDoColor = ($todo['completed'] === 'true') ? 'green' : 'red'; ?>
              <li style="color:<?= $toDoColor ?>"><?= $todo['id']; ?>: <?= $todo['title']; ?></li>

            <? } ?>
          </div>
        </div>
      
    </div>
  </div>
   <!-- echo "<pre>DEBUG";
  print_r($albums);
  echo "</pre>";  -->


</body>

</html>