<?
require_once 'bdd.inc.php';
$title = 'Album photos'; //Petite bricole pour gérer le contenu de la balise <title>, présente dans header.php
require_once 'partials/header.php';


//Vérification que l'album id est bien défini
$album_id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

if($album_id === false || $album_id === NULL){  
  exit("Erreur système (ligne ".__LINE__."): l'id de l'album doit etre définit.");
}

/** Infos sur l'album */
$album = runQuery('SELECT * FROM albums WHERE id = :album_id', [':album_id' => $album_id], false);


/** Infos sur l'utilisateur propriétaire de l'album */
$user = runQuery('SELECT * FROM users WHERE id = :user_id AND truc = :bidule', [':user_id' => $album->userId], false);


/** Récup de toutes les photos liées à l'album */
$photos = runQuery('SELECT * FROM photos WHERE albumid = :album_id', [':album_id' => $album_id]);
?>

<div class="container-fluid">

  <div class="my-3 border-bottom">
    <h2 class="text-center">Nom de l'album : <?=$album->title;?></h2>
    <h5 class="text-center">Propriétaire : <a href="user.php?id=<?=$user->id?>"><?=$user->name?></a></h5>

  </div>

  <div class="container-fluid">
    <div class="row d-flex justify-content-center">
      <? foreach ($photos as $photo) { ?>
        <div class="card m-1" style="width: 18rem;">
          <img src="<?=$photo->thumbnailUrl;?>" class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title"><?=$photo->title?></h5>
          </div>
          <div class="card-footer text-center">
            <a href="<?=$photo->url?>" class="btn btn-info" target="_blank">Voir la photo</a>
          </div>
        </div>
      <? } ?>
    </div>
  </div>

</div>
