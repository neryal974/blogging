<?
require_once 'bdd.inc.php';

$title = 'Articles'; //Petite bricole pour gérer le contenu de la balise <title>, présente dans header.php

require_once 'partials/header.php';



//Récupération de l'id avec filter_input
$post_id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

//Vérification que l'id user est bien défini
if($post_id === false || $post_id === NULL){
  exit("Erreur système (ligne ".__LINE__."): l'id de l'article doit etre définit.");
}

//On va chercher les infos sur l'article
$post = runQuery('SELECT * FROM posts WHERE id = :post_id', [':post_id' => $post_id], false);

// On va chercher les infos sur l'utilisateur propriétaire de l'article
$user = runQuery('SELECT * FROM users WHERE id = :user_id', [':user_id' => $post->userId], false);

//On va chercher tous les commentaires
$comments = runQuery('SELECT * FROM comments WHERE postid = :post_id', [':post_id' => $post_id]);
?>

<div class="container-fluid">

  <div class="my-3 border-bottom">
    <h5 class="text-center">Auteur : <a href="user.php?id=<?=$user->id?>"><?=$user->name?></a></h5>
  </div>

  <div class="container">
    <h2>Article</h2>
    <div class="card mb-5">
      <h5 class="card-header bg-success"><?=$post->title?></h5>
      <div class="card-body">
        <h5 class="card-title">#<?=$post->id?></h5>
        <p class="card-text"><?=$post->body?></p>
      </div>
    </div>
    <h2 class="text-right">Commentaires</h2>
    <? foreach ($comments as $comment) { ?>
      <div class="card ml-5 mb-2">
        <h5 class="card-header bg-info"><?=$comment->name?></h5>
        <div class="card-body">
          <h5 class="card-title">#<?=$comment->id?></h5>
          <p class="card-text"><?=$comment->body?></p>
        </div>
        <div class="card-footer text-right">
          <label class="text-left"><?=$comment->email?></label>
        </div>
      </div>
    <? } ?>

  </div>

</div>
