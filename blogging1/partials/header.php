<?
/*
Cette ligne sera toujours appelé dans la nav pour la liste des utilisateurs, donc autant la mettre ici
Pas de parametre à envoyer et on veut plusieurs résultats
On aurait pu écrire cela aussi : runQuery("SELECT * FROM users", [], true)
Mais les paramètres par défaut de la fonction runQuery nous permette de ne pas les spécifier dans ce cas la.
*/
$users = runQuery("SELECT * FROM users");
?>


<!DOCTYPE html>
 <html lang="fr" dir="ltr" class="h-100">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?=$title ?? 'Liste des Utilisateurs'; //Le title est dynamqie, géré au cas par cas selon les pages?></title>

    <!-- BOOTSTRAP 4.4.1 -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/style.css">
  </head>

  <body>
  <header>
    <div class="container-fluid p-0">
      <div class="jumbotron jumbotron-fluid">
        <div class="container bg-light">
          <h1 class="display-4 font-weight-bold">Plateforme de Blogging</h1>
          <p class="lead">Ceci est un exercice permettant de travailler PHP et PDO.</p>
        </div>
      </div>
    </div>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark text-center">
      <a class="navbar-brand" href="#">Blogging PDO</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="index.php">Accueil <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Voir les différents blogs
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <?foreach ($users as $k => $user) { //Affichage de la liste d'utilisateurs ?>
                <a class="dropdown-item" href="user.php?id=<?=$user->id?>"><?=$user->name?> (<?=$user->username?>)</a>
              <?}?>
            </div>
          </li>
        </ul>
      </div>
    </nav>

  </header>
