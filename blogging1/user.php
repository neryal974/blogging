<?
require_once 'bdd.inc.php';
require_once 'partials/header.php';

//Vérification que l'id user est bien défini

$user_id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);


if($user_id ===false || $user_id === NULL){
  exit("Erreur système (ligne ".__LINE__."): l'id utilisateur doit etre définit.");
}


$title = 'fiche Personnelle';

/** Récupération des tâches de l'utilisateur 
 * Ici on souhaite récupérer un seul utilisateur, donc le troisieme parametre vaut false
 * On n'oublie pas d'envoyer le tableau des params en deuxième argument
*/
$user = runQuery('SELECT * FROM users WHERE id = :user_id', [':user_id' => $user_id], false);


/** Récupération des tâches de l'utilisateur 
 * 
 * On veut récupérer plusieurs ligne, donc on garde la valeur par défaut de fetchMany dans la fonction runQuery
 * (donc pas de troisieme parametre)
 * 
*/
$todos = runQuery('SELECT * FROM todos WHERE userId = :user_id', [':user_id' => $user_id]);

/** Récupération des albums de l'utilisateur */
$albums = runQuery('SELECT * FROM albums WHERE userId = :user_id', [':user_id' => $user_id]);

/** Récupération des articles de l'utilisateur */
$posts = runQuery('SELECT * FROM posts WHERE userId = :user_id', [':user_id' => $user_id])
?>

<div class="container">
  <h2 class="text-center my-3">Profil de <span class="text-success"><?=$user->name;?></span></h2>
</div>

<div class="container mt-4">
  <div class="row d-flex justify-content-between">

    <div class="col-lg-5 shadow p-3 mb-5 bg-white rounded">
      <h4 class="bg-dark p-3 text-white text-center my-3">INFORMATIONS PERSONNELLES</h4>
      <dl class="row">
        <dt class="col-sm-3">Nom : </dt>
        <dd class="col-sm-9"><?=$user->name;?></dd>
        <dt class="col-sm-3">Login : </dt>
        <dd class="col-sm-9"><?=$user->username;?></dd>
        <dt class="col-sm-3">Email : </dt>
        <dd class="col-sm-9"><?=$user->email;?></dd>
        <dt class="col-sm-3">Adresse : </dt>
        <dd class="col-sm-9">
          <adress><?=$user->addressstreet. '<br>' .$user->addresssuite;?></adress><br>
          <adress><?=$user->addresszipcode.' '.$user->addresscity;?></adress>
        </dd>
        <dt class="col-sm-3">Téléphone : </dt>
        <dd class="col-sm-9"><?=$user->phone;?></dd>
        <dt class="col-sm-3">Entreprise : </dt>
        <dd class="col-sm-9">
          <ul>
            <li>Nom : <?=$user->companyname;?></li>
            <li>site : <a href="#"></><?=$user->website;?></a></li>
            <li>slogan : <?=$user->companycatchPhrase;?></li>
          </ul>
        </dd>
      </dl>
    </div>

    <div class="col-lg-6 shadow p-3 mb-5 bg-white rounded">
      <h4 class="bg-dark p-3 text-white text-center my-3">TODOS LIST</h4>
      <ul>
        <? foreach ($todos as $todo) { ?>
          <li class="<?=($todo->completed == 'true') ? 'text-success' : 'text-danger';?>">#<?=$todo->id?> <?=$todo->title;?></li>
        <? } ?>
      </ul>
    </div>

  </div>
</div>

<div class="container mt-4">
  <div class="row d-flex justify-content-between">
    <div class="col-lg-5 shadow p-3 mb-5 bg-white rounded">
      <h4 class="bg-dark p-3 text-white text-center my-3">ALBUMS PHOTOS</h4>
      <ul>
        <? foreach ($albums as $album){ ?>
          <li>#<?=$album->id?> <a href="album.php?id=<?=$album->id?>"><?=$album->title?></a></li>
        <? } ?>
      </ul>
    </div>
    <div class="col-lg-6 shadow p-3 mb-5 bg-white rounded">
      <h4 class="bg-dark p-3 text-white text-center my-3">ARTICLES</h4>
      <ul>
        <? foreach ($posts as $post): ?>
          <li>#<?=$post->id?> <a href="post.php?id=<?=$post->id?>"><?=$post->title?></a></li>
        <? endforeach;?>
      </ul>
    </div>
  </div>
</div>
