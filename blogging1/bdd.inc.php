<?

require_once 'env.php';

try{
  $db = new PDO("mysql:host=$db_hostname; dbname=$db_database;", $db_username, $db_password);
  $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
} catch (PDOException $e) {
  echo "Erreur!: " . $e->getMessage() . "<br/>";
  die();
}


/**
 * Cette fonction permet d'exécuter tout type de requete SQL de manière générique
 * 
 * $sql sera la requete SQL (ex : SELECT * FROM users)
 * $fetchMany nous permet de savoir si on doit renvoyer une seule ligne ou bien plusieurs
 */

function runQuery($sql, $params = [], $fetchMany = true){ // On peut définir une valeur par défaut pour les paramètres en PHP
  global $db; //Attention : on récupère la variable $db initialisé à l'extérieur de cette fonction (problème de scope)
  
  $query = $db->prepare($sql); //preparation de la requête
  $query->execute($params); //Ajout des paramètres + exécution de la requete

  if($fetchMany === false){ //Si on attend qu'un seul résultat
      return $query->fetch(); //On utilise fetch()
  } else { //Si on attends plusieurs résultats
      return $query->fetchAll(); //On utilise fetchAll()
  }
}

?>
