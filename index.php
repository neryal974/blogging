<? require_once 'dataBase.php';
require_once 'header.php';
require_once 'nav.php'; ?>


<body>

  <body>
    <div class="container" style="margin: 30px;">
      <div class="row justify-content-center">
        <div class="col-4">
          <p>Cet exercice permet de travailler PHP/PDO et les bases de données.
            Cliquez sur la liste dans la barre de navigation pour voir les différents blogs des utlisateurs de ce site.</p>
          <br><span style="text-decoration:underline"><b>Chaque utilisateur dispose de plusieurs "entitées" :</b></span>
          <br><br>
          <ul>
            <li>
              Informations personnelles
            </li>
            <li>
              Tâches Todos
            </li>
            <li>
              Album photos + photos
            </li>
            <li>
              Articles + commentaires
            </li>
          </ul>
          <p>A vous d'aller requêter la base de données pour afficher correctement tout cela, sur le modeèle de ce site.
            Vous pouvez personnaliser ou agrémenter ce que vous voulez tant que toutes les données sont accessibles facilement,
            et que vous suivez cet exemple.</p>
        </div>
        <div class="col-4">
          <img src="\photo\photo.jpg">
        </div>
      </div>
    </div>
  </body>
</body>