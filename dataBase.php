<?
require_once 'env.php';
require_once 'functionDebug.php';





//Bloc try/catch pour attraper l'erreur s'il y a un problème de connexion avec la DB
try {  
  $db = new PDO("mysql:host=$db_hostname; dbname=$db_database;", $db_username, $db_password); //On se connecte à la DB en utilisant les données présentes dans env.php
  $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC); //Je configure le fetch mode en tableaux associatif par défaut
} catch (PDOException $e) { //Si Exception (erreur) attrapée (catch) par PHP)  
  echo "Erreur!: " . $e->getMessage() . "<br/>"; //on affiche le message   
  die();}