<?


//Affiche les erreurs liées à une requete SQL (PDO errorInfo())
function pdo_error($q){
    echo "<h4>PDO ERROR :</h4>";
    echo "<pre>";
    print_r($q->errorInfo());
    echo "</pre>";
}

//Affiche le contenu d'une variable
function debug($var){
    echo "<pre>DEBUG";
    print_r($var);
    echo "</pre>";
}